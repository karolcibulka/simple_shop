<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4><?=lang('errors.phpError')?></h4>

<p><?=lang('errors.severity')?>: <?php echo $severity; ?></p>
<p><?=lang('errors.message')?>:  <?php echo $message; ?></p>
<p><?=lang('errors.file')?>: <?php echo $filepath; ?></p>
<p><?=lang('errors.line')?>: <?php echo $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

	<p><?=lang('errors.backtrace')?>:</p>
	<?php foreach (debug_backtrace() as $error): ?>

		<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

			<p style="margin-left:10px">
				<?=lang('errors.file')?>: <?php echo $error['file'] ?><br />
				<?=lang('errors.line')?>: <?php echo $error['line'] ?><br />
				<?=lang('errors.function')?>: <?php echo $error['function'] ?>
			</p>

		<?php endif ?>

	<?php endforeach ?>

<?php endif ?>

</div>