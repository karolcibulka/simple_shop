<!doctype html>
<html class="no-js" lang="en">

<head>

</head>

<body>
    @include('partials.header')

    @yield('content')
    
    @include('partials.footer')
</body>

</html>