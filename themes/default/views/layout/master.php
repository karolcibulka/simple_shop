<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=isset($title) ? $title : ''?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    
    <!-- CSS 
    ========================= -->
    <!--bootstrap min css-->
    <link rel="stylesheet" href="<?=Template::asset('css/bootstrap.min.css')?>">
    <!--owl carousel min css-->
    <link rel="stylesheet" href="<?=Template::asset('css/owl.carousel.min.css')?>">
    <!--slick min css-->
    <link rel="stylesheet" href="<?=Template::asset('css/slick.css')?>">
    <!--magnific popup min css-->
    <link rel="stylesheet" href="<?=Template::asset('css/magnific-popup.css')?>">
    <!--font awesome css-->
    <link rel="stylesheet" href="<?=Template::asset('css/font.awesome.css')?>">
    <!--ionicons css-->
    <link rel="stylesheet" href="<?=Template::asset('css/ionicons.min.css')?>">
    <!--linearicons css-->
    <link rel="stylesheet" href="<?=Template::asset('css/linearicons.css')?>">
    <!--animate css-->
    <link rel="stylesheet" href="<?=Template::asset('css/animate.css')?>">
    <!--jquery ui min css-->
    <link rel="stylesheet" href="<?=Template::asset('css/jquery-ui.min.css')?>">
    <!--slinky menu css-->
    <link rel="stylesheet" href="<?=Template::asset('css/slinky.menu.css')?>">
    <!--plugins css-->
    <link rel="stylesheet" href="<?=Template::asset('css/plugins.css')?>">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?=Template::asset('css/style.css')?>">
    
    <!--modernizr min js here-->
    <script src="<?=Template::asset('js/vendor/modernizr-3.7.1.min.js')?>"></script>

    <style>
        .search_container .nice-select{
            min-width:200px;
        }
    </style>
</head>

<body>
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
    @include('partials.modal')
    @include('partials.popup')
    

    
<!-- JS
============================================ -->
<!--jquery min js-->
<script src="<?=Template::asset('js/vendor/jquery-3.4.1.min.js')?>"></script>
<!--popper min js-->
<script src="<?=Template::asset('js/popper.js')?>"></script>
<!--bootstrap min js-->
<script src="<?=Template::asset('js/bootstrap.min.js')?>"></script>
<!--owl carousel min js-->
<script src="<?=Template::asset('js/owl.carousel.min.js')?>"></script>
<!--slick min js-->
<script src="<?=Template::asset('js/slick.min.js')?>"></script>
<!--magnific popup min js-->
<script src="<?=Template::asset('js/jquery.magnific-popup.min.js')?>"></script>
<!--counterup min js-->
<script src="<?=Template::asset('js/jquery.counterup.min.js')?>"></script>
<!--jquery countdown min js-->
<script src="<?=Template::asset('js/jquery.countdown.js')?>"></script>
<!--jquery ui min js-->
<script src="<?=Template::asset('js/jquery.ui.js')?>"></script>
<!--jquery elevatezoom min js-->
<script src="<?=Template::asset('js/jquery.elevatezoom.js')?>"></script>
<!--isotope packaged min js-->
<script src="<?=Template::asset('js/isotope.pkgd.min.js')?>"></script>
<!--slinky menu js-->
<script src="<?=Template::asset('js/slinky.menu.js')?>"></script>
<!--instagramfeed menu js-->
<script src="<?=Template::asset('js/jquery.instagramFeed.min.js')?>"></script>
<!-- Plugins JS -->
<script src="<?=Template::asset('js/plugins.js')?>"></script>

<!-- Main JS -->
<script src="<?=Template::asset('js/main.js')?>"></script>
<script src="<?=Template::asset('js/custom.js')?>"></script>



</body>

</html>