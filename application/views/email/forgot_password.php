		<div align="center" style="text-align: left;border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
			<div style="padding-top: 10px;padding-left:10px;">
				<p>Dobrý deň <b><?=$user->u_Name?> <?=$user->u_Surname?></b><br/>
					Práve ste požiadali o <b>obnovenie</b> zabudnutého hesla.<br />
					Pre zadanie nového hesla, prosím kliknite na nasledujúci link:<br />
				</p>
				<p>
					<b><a href="<?=base_url()?>auth/reset_password/<?=$uid?>/<?=$new_pass_key?>" style="color: #3366cc;">Pokračovať v registrácii...</a></b>
				</p>
				Ak link nefunguje, prosím zkopírujte ho do Vášho prehliadača:<br />
				<?=base_url()?>auth/reset_password/<?=$uid?>/<?=$new_pass_key?><br /><br />
				<b>Zmenu dokončite do 24 hodín V opačnom prípade bude tento odkaz neplatný!</b>
			</div>
		</div>