		<div align="center" style="text-align: left;border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
			<div style="padding-top: 10px;padding-left:10px;">
				<p>Vitajte <b><?=$user->u_Name?> <?=$user->u_SurName?></b><br/>
					Vaše konto bolo úspešne vytvorené, ale ešte nebolo <b>aktivované</b>.<br />
					Pre aktiváciu konta pokračujte prosím kliknutím na naledujúci link:<br />
				</p>
				<p>
					<b><a href="<?=base_url()?>auth/activate/<?=$uid?>/<?=$new_email_key?>" style="color: #3366cc;">Potvrdiť Email</a></b>
				</p>
				Ak link nefunguje, prosím zkopírujte ho do Vášho prehliadača:<br />
				<?=base_url()?>auth/activate/<?=$uid?>/<?=$new_email_key?><br /><br />
				<b>Registráciu dokončite do 24 hodín V opačnom prípade bude táto pozvánka neplatná!</b>
			</div>
		</div>