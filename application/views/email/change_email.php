		<div align="center" style="text-align: left;border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
			<div style="padding-top: 10px;padding-left:10px;">
				<p>Dobrý deň<br/>
					Váš email bude <b>zmenený</b>.<br />
					Pre potvrenie emailovej adresy kliknutím na naledujúci link:<br />
				</p>
				<p>
					<b><a href="<?=base_url()?>auth/reset_email/<?=$uid?>/<?=$new_email_key?>" style="color: #3366cc;">Pokračovať v registrácii...</a></b>
				</p>
				Ak link nefunguje, prosím zkopírujte ho do Vášho prehliadača:<br />
				<?=base_url()?>auth/reset_email/<?=$uid?>/<?=$new_email_key?><br />
				<br />
				<br>
				Vaša nová emailová adresa: <?=$new_email; ?><br />
				<br />
				Tento email Vám bol odoslaný na základe požiadavky na <a href="<?=base_url();?>" style="color: #3366cc;"><?=$site_name?></a> Ak ste nežiadali o zmenu emailu, ignorujte tento email. Po krátkom čase bude tento link naktívny.<br />
			</div>
		</div>