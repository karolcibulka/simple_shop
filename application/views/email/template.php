<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="<?=$lang_index?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
		<title><?=$mail_subject?></title> 
	</head>
	<body align="center" style="
		font-size:14px;
		font-family:Arial;
		min-height: 50px;
		max-width: 700px;
		padding: 10px;
		margin-left: auto;
		margin-right: auto;
		background-color: #efefef;
		border: 1px solid #9e9e9e;
		border-radius: 4px;
		box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);
		">
		<div align="center">
			<div align="center" style="border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
				<h3 style="margin: 0;text-align:center;"><b><?=$mail_subject?></b></h3>
			<?php if(is_file('./assets/img/logo_mail.jpg')):?>
				<img align="center"src="<?= base_url();?>/assets/img/email/h3_bar.PNG" style="  margin-left: 5px;width: 200px;height: 6px;"/>
			<?php endif;?>
			</div>
			<div align="center">
	            <?=$mail_message?>
			</div>		
			<div align="center" style="text-align: left;border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
				<p><a style="color: #5f5f5f;text-decoration: none;padding-left:20px;">S pozdravom tím <?=$site_name?></a></p>
			</div>
		</div>
	</body>
</html>