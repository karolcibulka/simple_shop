		<div align="center" style="text-align: left;border-radius: 7px;padding-top: 5px;padding-bottom: 5px;margin-top: 5px;margin-bottom: 5px;border:1px solid #c3c3c3;box-shadow: 1px 1px 4px 0px rgba(0, 0, 0, 0.36);background-color: #fdfdfd;">
			<div style="padding-top: 10px;padding-left:10px;">
				<p>Dobrý deň <br/>
					Používateľ <b><?=$user->u_Name?> <?=$user->u_Surname?></b> Vás pozýva do svojej skupiny <b><?=$agency->ha_Name?></b><br />
					Pre dokončenie registrácie pokračujte prosím kliknutím na naledujúci link:<br />
				</p>
				<p>
					<b><a href="<?=base_url()?>auth/register/<?=$invitation->ui_Hash?>" style="color: #3366cc;">Zaregistrovať sa</a></b>
				</p>
				Ak link nefunguje, prosím zkopírujte ho do Vášho prehliadača:<br />
				<?=base_url()?>auth/register/<?=$invitation->ui_Hash?><br /><br />
				<b>Registráciu dokončite do <?=date('d.n.Y', strtotime($invitation->ui_ExpirationDate))?> V opačnom prípade bude táto pozvánka neplatná!</b>
			</div>
		</div>