<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>Vyskytla sa chyba PHP</h4>

<p>Severity: <?php echo $severity; ?></p>
<p>Správa:  <?php echo $message; ?></p>
<p>Súbor: <?php echo $filepath; ?></p>
<p>Riadok: <?php echo $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

	<p>Spätné volania:</p>
	<?php foreach (debug_backtrace() as $error): ?>

		<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

			<p style="margin-left:10px">
			Súbor: <?php echo $error['file'] ?><br />
			Riadok: <?php echo $error['line'] ?><br />
			Funkcia: <?php echo $error['function'] ?>
			</p>

		<?php endif ?>

	<?php endforeach ?>

<?php endif ?>

</div>