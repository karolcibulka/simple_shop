<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function pre_r($expression, $return = false){
		if ($return)
		{
		  if (is_string($expression)) {
		      return '<pre>' . print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), true) . '</pre>';
          }
			return '<pre>' . print_r($expression, true) . '</pre>';
		}

			echo '<pre>';
			if (is_string($expression)) {
                print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), false);
            }
			else{
			    print_r($expression, false);
            }
			echo '</pre>';
	}

    function translate_with_count($val1,$lang_key,$val2 = false){

	    if($lang_key){
	        if($val2){
                if($val1 === $val2){
                    return translate_with_count($val1,$lang_key,false);
                }

                if($val2 > 0 && $val2 < 2){
                    return $val1.'-'.$val2.' '.lang($lang_key.'1');
                }

                if($val2 >= 2 && $val2<5){
                    return $val1.'-'.$val2.' '.lang($lang_key.'2');
                }

                return $val1.'-'.$val2.' '.lang($lang_key.'5');

            }

            if($val1 > 0 && $val1 < 2){
                return $val1.' '.lang($lang_key.'1');
            }
            
            if($val1 >= 2 && $val1<5){
                return $val1.' '.lang($lang_key.'2');
            }

            return $val1.' '.lang($lang_key.'5');
        }
    }

    function shortcut_to_lang($lang){
	    switch($lang){
            case 'sk':
                return 'slovak';
                break;
            case 'fr':
                return 'france';
                break;
            case 'en':
                return 'english';
                break;
            case 'de':
                return 'deutsch';
                break;
        }
    }
?>