<?php

function isLoggedIn(){
    $ci = & get_instance();

    if($ci->session->has_userdata('customer')){
        $customer = $ci->session->userdata('customer');
        return isset($customer['user_id'],$customer['user_token']);
    }

    return false;
}