<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 *                         * ************** for Controllers *****************
 *============ Codeigniter Core System ================
 * @property CI_Benchmark $benchmark              Benchmarks
 * @property CI_Config $config                    This class contains functions that enable config files to be managed
 * @property CI_Controller $controller            This class object is the super class that every library in.
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property CI_Hooks $hooks                      Provides a mechanism to extend the base system without hacking.
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Loader $load                      Loads views and files
 * @property CI_Log $log                          Logging Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Profiler $profiler                Display benchmark results, queries you have run, etc
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_URI $uri                          Retrieve information from URI strings
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 *
 *
 * @property CI_Model $model                      Codeigniter Model Class
 *
 * @property CI_Driver $driver                    Codeigniter Drivers
 *
 *
 *============ Codeigniter Libraries ================
 *
 * @property CI_Cache $cache                      Caching
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encryption $encryption            The Encryption Library provides two-way data encryption.
 * @property CI_Upload $upload                    File Uploading class
 * @property CI_Form_validation $form_validation  Form Validation class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Migration $migration              Tracks & saves updates to database structure
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Template parser
 * @property CI_Security $security                Processing input data for security.
 * @property CI_Session $session                  Session Class
 * @property CI_Table $table                      HTML table generation
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 *
 *
 *                          *============ Database Libraries ================
 *
 *
 * @property CI_DB_query_builder $db   Database
 * @property CI_DB_forge $dbforge     Database
 * @property CI_DB_result $result                 Database
 *
 *
 *
 *
 *                            *============ Codeigniter Depracated  Libraries ================
 *
 * @property CI_Javascript $javascript            Javascript (not supported
 * @property CI_Jquery $jquery                    Jquery (not supported)
 * @property CI_Encrypt $encrypt                  Its included but move over to new Encryption Library
 *
 *            ======= MY PROPERTIES =======
 * @property Template $template                  Its included but move over to new Encryption Library
 * @property curl $curl                  Its included but move over to new Encryption Library
 * @property Responser $responser                  Its included but move over to new Encryption Library
 *
 *
 */
class MY_Controller extends CI_Controller
{

    protected $theme_config;
    protected $api_key;
    protected $current_lang;
    protected $web_service_endpoint;
    protected $development_version = false;
    protected $cache_store_length = 3600 * 24 * 7;
    protected $property_id;

    protected $property_data;
    protected $categories;
    protected $recipes;

    public function __construct()
    {

        parent::__construct();

        $this->load->config('theme');
        $this->theme_config = $this->config->item('theme');

        //load theme_config data to protected variables
        $this->web_service_endpoint = $this->theme_config['web_service_endpoint'];
        $this->api_key              = $this->theme_config['api_key'];
        $this->property_id          = $this->theme_config['property_id'];

        //get current_lang from first url segment
        $this->current_lang = $this->uri->segment(1);
        $this->load->library('Response', array('language'=>$this->current_lang), 'response');

        if ($this->development_version) {
            $this->web_service_endpoint = $this->theme_config['dev_web_service_endpoint'];
        }

        //load language file
        $this->lang->load(array('default'));

        //load cache driver
        $this->load->driver('cache', array(
            'adapter' => 'file',
            'backup' => 'file',
            'key_prefix' => $this->property_id . '_'
        ));

        //set theme to choice actual version if version !== 'default' && exist theme_config['version]  ~/themes/$this->theme_config['version']
        if (isset($this->theme_config['version']) && $this->theme_config['version'] !== 'default') {
            Template::setTheme($this->theme_config['version']);
        }
        Template::useBlade();

        // if exist get param deleteCache empty cached data and basket data
        if (isset($_GET['deleteCache'])) {
            $this->cache->clean();
            Basket::clean();
        }

        //if (!$this->property_data = $this->getData('property_data')) {
        //    exit('property_data');
        //}

        //$this->categories = $this->getData('categories');
        //$this->recipes    = $this->getData('recipes');

        Template::set('property', $this->property_data);
        Template::set('categories', $this->categories);
        Template::set('recipes', $this->recipes);
        Template::set('current_language', $this->current_lang);
    }

    //curl call to getData from endpoint
    private function getData($name)
    {
        if (!$response = $this->cache->get($name)) {
            $this->curl->create($this->getEndpoint($name));
            $this->curl->http_header('API-KEY', $this->api_key);
            $api_response = json_decode($this->curl->execute(), true);
            if (isset($api_response['data'], $api_response['status']) && !empty($api_response['data'])) {
                $response = Response::$name($api_response['data']);
                $this->cache->save($name, $response, $this->cache_store_length);
            }
        }

        return $response;
    }

    //return endpoint for curl calls
    private function getEndpoint($name)
    {
        switch ($name) {
            case 'property_data':
                return $this->web_service_endpoint . 'property/getProperty/' . $this->property_id;
                break;
            case 'categories':
                return $this->web_service_endpoint . 'property/getCategories/' . $this->property_id;
                break;
            case 'recipes':
                return $this->web_service_endpoint . 'property/getRecipes/' . $this->property_id;
                break;
            default:
                return null;
                break;
        }
    }

}
