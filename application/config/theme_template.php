<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['theme']['version'] = 'default';
$config['theme']['property_id'] = '1';
$config['theme']['web_service_endpoint'] = 'http://localhost/warehouse';
$config['theme']['api_key'] = 'api_key';

$config['theme']['development'] = true;
$config['theme']['dev_web_service_endpoint'] = 'http://localhost/warehouse';
