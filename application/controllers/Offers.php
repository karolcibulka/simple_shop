<?php


class Offers extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug_category,$slug_offer = null){
        if(!$slug_category){
            redirect('/');
        }

        if(!$category = $this->getCategory($slug_category)){
            redirect('/');
        }

        Template::set('category',$category);

        if($slug_offer){
            if(!$offer = $this->getOffer($category,$slug_offer)){
                redirect(base_url($this->current_lang.'/'.$slug_category));
                //with message ??
            }


            Template::set('recipes',Response::getRecipesByOffer($offer,$this->recipes));
            Template::set('offer',$offer);
            Template::view('offers/offer');
        }
        else{
            Template::view('offers/offers');
        }
    }

    private function getOffer($category,$slug_offer){
        if(isset($category['offers']) && !empty($category['offers'])){
            foreach($category['offers'] as $offer){
                if(isset($offer['languages'][$this->current_lang]['slug']) && $offer['languages'][$this->current_lang]['slug'] === $slug_offer){
                    return $offer;
                }
            }
        }

        return array();
    }

    private function getCategory($slug_category){
        if(!empty($this->categories)){
            foreach($this->categories as $category){
                if(isset($category['languages'][$this->current_lang]['slug']) && $category['languages'][$this->current_lang]['slug'] === $slug_category ){
                    return $category;
                }
            }
        }

        return array();
    }
}