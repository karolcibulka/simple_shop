<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author    CodeIgniter community
 * @author    Gabriel Potkány <gadelat+codeigniter@gmail.com>
 * @copyright Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license   http://opensource.org/licenses/MIT MIT License
 * @link      http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']              = 'Pole <b>{field}</b> je povinné.';
$lang['form_validation_isset']                 = 'Pole <b>{field}</b> musí obsahovať hodnotu.';
$lang['form_validation_valid_email']           = 'Pole <b>{field}</b> musí obsahovať platnú emailovú adresu.';
$lang['form_validation_valid_emails']          = 'Pole <b>{field}</b> musí obsahovať platné emailové adresy.';
$lang['form_validation_valid_url']             = 'Pole <b>{field}</b> musí obsahovať platnú URL.';
$lang['form_validation_valid_ip']              = 'Pole <b>{field}</b> musí obsahovať platnú IP.';
$lang['form_validation_min_length']            = 'Pole <b>{field}</b> musí obsahovať aspoň {param} znakov.';
$lang['form_validation_max_length']            = 'Pole <b>{field}</b> nesmie obsahovať viac než {param} znakov.';
$lang['form_validation_exact_length']          = 'Pole <b>{field}</b> musí obsahovať presne {param} znakov.';
$lang['form_validation_alpha']                 = 'Pole <b>{field}</b> môže obsahovať iba znaky abecedy.';
$lang['form_validation_alpha_numeric']         = 'Pole <b>{field}</b> môže obsahovať iba znaky abecedy a čísla.';
$lang['form_validation_alpha_numeric_spaces']  = 'Pole <b>{field}</b> môže obsahovať iba znaky abecedy, čísla a mezery.';
$lang['form_validation_alpha_dash']            = 'Pole <b>{field}</b> môže obsahovať iba znaky abecedy, čísla, podtržítka a pomlčky.';
$lang['form_validation_numeric']               = 'Pole <b>{field}</b> môže obsahovať iba čísla.';
$lang['form_validation_is_numeric']            = 'Pole <b>{field}</b> môže obsahovať iba číselné znaky.';
$lang['form_validation_integer']               = 'Pole <b>{field}</b> musí být celé číslo.';
$lang['form_validation_regex_match']           = 'Pole <b>{field}</b> nie je v správnom formáte.';
$lang['form_validation_matches']               = 'Pole <b>{field}</b> nie je zhodné s poľom {param}.';
$lang['form_validation_differs']               = 'Pole <b>{field}</b> musí byť rôzne od poľa {param}.';
$lang['form_validation_is_unique']             = 'Pole <b>{field}</b> musí obsahovať unikátnu hodnotu.';
$lang['form_validation_is_natural']            = 'Pole <b>{field}</b> môže obsahovať iba prirodzené čísla a nulu.';
$lang['form_validation_is_natural_no_zero']    = 'Pole <b>{field}</b> môže obsahovať iba prirodzené čísla.';
$lang['form_validation_decimal']               = 'Pole <b>{field}</b> musí obsahovať desatinné čislo.';
$lang['form_validation_less_than']             = 'Pole <b>{field}</b> musí být menšie než pole {param}.';
$lang['form_validation_less_than_equal_to']    = 'Pole <b>{field}</b> musí být menšie alebo rovnaké ako pole {param}.';
$lang['form_validation_greater_than']          = 'Pole <b>{field}</b> musí být väčšie než pole {param}.';
$lang['form_validation_greater_than_equal_to'] = 'Pole <b>{field}</b> musí být väčšie alebo rovnaké ako pole {param}.';
$lang['form_validation_error_message_not_set'] = 'Pre pole <b>{field}</b> nie je nastavená chybová hláška.';
$lang['form_validation_in_list']               = 'Pole <b>{field}</b> musí být jedným z: {param}.'; 
