<?php

class Blade{

    private static $ci;
    private static $cache;
    private static $data;
    private static $template;
    private static $compilers;
    private static $cache_path;
    private static $content;
    private static $last_section = array();
    private static $sections;

    private static $has_master = false;

    public function __construct($data)
    {
        self::$ci = & get_instance();
        self::$template = $data['template'];

        self::$cache_path = APPPATH.'cache/rendered_views/'.self::$template.'/';

        if(!file_exists(self::$cache_path)){
            mkdir(self::$cache_path,0777,true);
        }

        self::$compilers = array(
            'extensions',
            'yields',
            'ies',
            'sections',
            'sections_end',
            'foreaches',
            'endforeaches',
            'includes',
            'comments',
            'echos',
            'ifs',
            'elses',
            'endifs',
            'unless',
            //'forelse',
        );
    }

    public static function render($view){

        $view = str_replace('.','/',$view);

        if(!$rendered_view = self::getCachedView($view)){
            $rendered_view = self::parseView($view);
            self::storeView($view,$rendered_view);
        }


        self::$data['hovno'] = array(
            'prve_hovno',
            'druhe_hovno'
        );
        self::$data['cicina'] = 'cicina';

        self::run($rendered_view);
    }


    private static function parseView($view){

        self::$content = self::loadPHP($view);

        $internal_content = self::$content;

        foreach(self::$compilers as $compile){
            $internal_content = self::$compile($internal_content);
        }

        return $internal_content;
    }

    private static function run($content,$return = false){

        if (isset(self::$data) && is_array(self::$data))
        {
            extract(self::$data);
        }

		ob_start();

        eval(' ?>' . $content . '<?php ');

		if ($return)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}
        else{
            $final_content = ob_get_contents();
            @ob_end_clean();

            ob_start();
            eval(' ?>' . $final_content . '<?php ');

        }

        self::$ci->output->append_output(ob_get_contents());

        $content = ob_get_clean();

    }

    private static function partial($extension){
        $extension = str_replace('.','/',$extension);
        $master = self::loadPHP($extension);

        $internal_content = $master;
        foreach(self::$compilers as $compile){
            $internal_content = self::$compile($internal_content);
        }

        return $internal_content;
    }

    private static function get_section($section){
        if(isset(self::$sections[$section]) && !empty(self::$sections[$section])){
            return self::$sections[$section];
        }

        return '';
    }

    private static function extensions($content){
        $pattern = "/@extends\(\'(.*)\'\)/";
        return preg_replace($pattern, '<?php echo self::partial("$1","master") ; ?>', $content);
    }

    private static function yields($content){
        return preg_replace(self::matcher('yield'), '$1{{ self::get_section$2 }}', $content);
    }

    private static function includes($content){
        $pattern = "/@include\(\'(.*)\'\)/";
        return preg_replace($pattern, '<?php echo self::partial("$1","include") ; ?>', $content);
    }

    private static function matcher($function){
        return '/(\s*)@' . $function . '(\s*\(.*\))/';

    }

    private static function ies($content){
        return preg_replace(self::matcher('ie'), '$1<?php if( isset$2 && !empty$2 ):?>', $content);
    }

    private static function foreaches($content){
        $pattern = "/@foreach\((.*)\)/";
        //pre_r();exit;
        return preg_replace($pattern, '<?php foreach( $1 ):?>', $content);
    }

    private static function endforeaches($content){
        return str_replace('@endforeach', '<?php endforeach;?>', $content);
    }

    private static function section_start($section)
    {
        array_push(self::$last_section, $section);
        ob_start();

    }

    private static function section_end()
    {
        $last = array_pop(self::$last_section);
        self::section_extends($last, ob_get_clean());
        return $last;
    }

    protected static function section_extends($section, $content)
    {
        if (isset(self::$sections[$section]))
        {
            self::$sections[$section] = str_replace('@parent', $content, self::$sections[$section]);
        }
        else
        {
            self::$sections[$section] = $content;
        }
    }

    private static function sections($content){
        return preg_replace(self::matcher('section'), '$1<?php self::section_start$2; ?>', $content);
    }

    private static function sections_end($value)
    {
        $replace = '<?php self::section_end(); ?>';

        return str_replace('@endsection', $replace, $value);
    }



    private static function echos($content){

        return preg_replace('/\{\{(.+?)\}\}/', '<?php echo $1; ?>', $content);
    }

    private static function unless($content){

        $pattern = '/(\s*)@unless(\s*\(.*\))/';
        return preg_replace($pattern, '$1<?php if( ! $2): ?>', $content);
    }

    private static function comments($content){
        $content = preg_replace('/\{\{--(.+?)(--\}\})?\n/', "<?php // $1 ?>", $content);
        return preg_replace('/\{\{--((.|\s)*?)--\}\}/', "<?php /* $1 */ ?>\n", $content);
    }

    private static function ifs($content){
        $pattern = '/@if(\(.*\))/';
        return preg_replace($pattern, '<?php if $1 : ?>', $content);
    }

    private static function endifs($content){
        return str_replace('@endif','<?php endif;?>',$content);
    }

    private static function elses($content){
        return str_replace('@else','<?php else:?>',$content);
    }

    //end compiles

    private static function getCachedView($view){
        if(file_exists(self::$cache_path.$view.'.rt')){
            return file_get_contents(self::$cache_path.$view.'.rt');
        }

        return null;
    }


    private static function loadPHP($view){
        return file_get_contents(APPPATH.'../themes/'.self::$template.'/views/'.$view.'.php');
    }

    private static function storeView($view,$content){
        file_put_contents(self::$cache_path.$view.'.rt',$content);
    }
}