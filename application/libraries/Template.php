<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Template {

	private static $theme = 'default';
    private static $layout = 'views/layout/master';
	private static $layout_data = array();
	private static $ci;
	private static $use_blade = false;

	public function __construct()
	{
		self::$ci = & get_instance();

		//self::setTheme(self::$theme);
		
	}

	public static function set($name, $value)
	{
		self::$layout_data[$name] = $value;
	}

	public static function setTheme($theme){
		self::$theme = $theme;
		//self::loadBlade();

		return new self;
	}

	public static function useBlade(){
		self::$use_blade = true;
		self::$ci->load->library('Blade',array('template'=>self::$theme));
	}

    public static function setLayout($layout){
		self::$layout = 'views/layout/' . $layout;
		return new self;
	}

	public static function asset($layout){
		return base_url('themes/'.self::$theme.'/assets/'.$layout);
	}


    public static function load($template = '', $view = '', $view_data = array())
	{

		self::set('content', self::$ci->load->view($view, $view_data, TRUE));

		return self::$ci->load->view($template, self::$layout_data);
	}


	public static function view($view = '', $view_data = array())
	{
		if(self::$use_blade){
			Blade::render($view);
		}
		else{
			self::set('content', self::$ci->load->view(self::$theme . '/views/' . $view, array_merge($view_data, self::$layout_data) , TRUE));

			return self::$ci->load->view(self::$theme .'/'. self::$layout, self::$layout_data);
		}
	}

	public static function partial($view = '', $view_data = array())
	{
		return self::$ci->load->view(self::$theme . '/views/partials/' . $view, array_merge($view_data, self::$layout_data) , TRUE);
	}

	public static function content(){
		return self::$layout_data['content'];
	}

	public static function banner($view = '', $view_data = array())
	{
		return self::$ci->load->view(self::$theme . '/views/banners/' . $view, array_merge($view_data, self::$layout_data) , TRUE);
	}

	public static function getView($view = '', $view_data = array())
	{
		return self::$ci->load->view(self::$theme . '/views/' . $view, array_merge($view_data, self::$layout_data), TRUE);
	}

}