<?php


class Basket
{

    protected static $basket_name;
    protected static $ci;
    protected static $basket_content;

    public function __construct()
    {
        self::$ci = &get_instance();

        self::$basket_content = array();

        static::$basket_name = 'basket_content';

        if(self::$ci->session->has_userdata(static::$basket_name)){
            self::$basket_content = self::$ci->session->userdata(static::$basket_name);
        }
        else{
            self::$ci->session->set_userdata(static::$basket_name,self::$basket_content);
        }
    }

    public static function getBasket(){
        return self::$basket_content;
    }

    public static function storeItem( $item_id, $item , $count = 1 ){

        if(isset( $basket_content['items'][$item_id] ) && !empty( $basket_content['items'][$item_id] )){
            $basket_content['items'][$item_id]['count'] += 1;
        }
        else{
            self::$basket_content['items'][$item_id] = $item;
            self::$basket_content['items'][$item_id]['count'] = $count;
        }

        self::save();
    }

    public static function getView(){
        return Template::getView('partials/basket',self::getBasket());
    }

    public static function save(){
        self::$ci->session->set_userdata(static::$basket_name,self::$basket_content);
    }

    public static function clean(){
        self::$ci->session->unset_userdata(static::$basket_name);
    }
}