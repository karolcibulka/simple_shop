<?php


class Response
{
    protected $language;

    public function __construct($params)
    {
        $this->language = $params['language'];
    }

    public static function categories($categories){
        return $categories;
    }

    public static function recipes($recipes){
        return $recipes;
    }

    public static function getRecipesByOffer($offer,$recipes){
        return $recipes;
    }
}